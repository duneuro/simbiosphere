from distutils.core import setup, Extension
import os

here = os.path.abspath(os.path.dirname(__file__))

module1 = Extension('simbiopy',
                    sources = [os.path.join('src','simbiopy.cc'),
                               os.path.join('src','analytic_solution.cc'),
                               os.path.join('src','external','simbio','Error.cpp'),
                               os.path.join('src','external','simbio','IP_SIM_SimulatorEEGSpheres_c.cpp'),
                               os.path.join('src','external','simbio','IP_SIM_ParameterSimulator_c.cpp'),
                               os.path.join('src','external','simbio','IP_SIM_AbstractSimulatorEEGMEG_c.cpp'),
                               os.path.join('src','external','simbio','IP_SIM_AbstractSimulator_c.cpp'),
                               os.path.join('src','external','simbio','IP_SIM_SensorConfiguration_c.cpp'),
                               ],
                    include_dirs = [os.path.join(here,'src','external','pybind11','include'),
                                    os.path.join(here,'src','external','simbio','include'),
                                    os.path.join(here,'src','include')], 
                    extra_compile_args=['-std=c++11'])

setup (name = 'simbiopy',
       version = '0.1',
       description = 'EEG analytical solution in a multi-layer sphere model',
       ext_modules = [module1])

#ifndef SIMBIOSPHERE_EXCEPTIONS_HH
#define SIMBIOSPHERE_EXCEPTIONS_HH

#include <exception>

namespace simbiosphere
{
  struct IllegalArgumentException : public std::exception {
    explicit IllegalArgumentException(const std::string& msg) : msg_(msg)
    {
    }
    virtual ~IllegalArgumentException()
    {
    }
    virtual const char* what() const noexcept override
    {
      return msg_.c_str();
    }
    const std::string msg_;
  };
}

#endif // SIMBIOSPHERE_EXCEPTIONS_HH

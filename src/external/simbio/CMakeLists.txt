set(SimBio_SOURCES Error.cpp
  IP_SIM_SimulatorEEGSpheres_c.cpp
  IP_SIM_ParameterSimulator_c.cpp
  IP_SIM_AbstractSimulatorEEGMEG_c.cpp
  IP_SIM_AbstractSimulator_c.cpp
  IP_SIM_SensorConfiguration_c.cpp)

include_directories(${CMAKE_CURRENT_SOURCE_DIR}/include)
add_library(simbio SHARED ${SimBio_SOURCES})

#ifndef SIMBIOSPHERE_ANALYTIC_SOLUTION_HH
#define SIMBIOSPHERE_ANALYTIC_SOLUTION_HH

#include <array>
#include <vector>

namespace simbiosphere
{
  std::vector<double> analytic_solution(const std::array<double, 4>& radii,
                                        const std::array<double, 3>& center,
                                        const std::array<double, 4>& conductivities,
                                        const std::vector<std::array<double, 3>>& electrodes,
                                        const std::array<double, 3>& dipoleposition,
                                        const std::array<double, 3>& dipolemoment);
}

#endif // SIMBIOSPHERE_ANALYTIC_SOLUTION_HH

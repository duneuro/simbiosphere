#include <pybind11/pybind11.h>
#include <pybind11/stl.h>

#include <simbiosphere/analytic_solution.hh>

namespace py = pybind11;

inline void register_analytic_solution(py::module& m)
{
  m.def("analytic_solution", simbiosphere::analytic_solution, "bla", py::arg("radii"),
        py::arg("center"), py::arg("conductivities"), py::arg("electrodes"),
        py::arg("dipoleposition"), py::arg("dipolemoment"));
}

PYBIND11_PLUGIN(simbiopy)
{
  py::module m("simbiopy", "simbiopy library");
  register_analytic_solution(m);
  return m.ptr();
}

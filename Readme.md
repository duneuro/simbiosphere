SimBioSphere
============

This library encasulates the analytical solution of the EEG forward problem
provided by the SimBio library ([wiki][1], [code][2]). We provide a pure C++
interface along with python bindings using pybind11 ([github][3]).

License
=======

The license for the simbio library can be found in the `LICENSE_SIMBIO.pdf` file
and on top of each simbio source file. The external pybind11 library is licensed
under a BSD-style license which can be found in the LICENSE file in the
respective sub module directory (src/external/pybind11). The remaining files are
licensed under an MIT license (see LICENSE).

Prerequisites
=============

If you want to use the python bindings, you need to have the python library
installed on your system. Under Debian or Ubuntu, this can be achieved by
installing the `python-dev` package.

Installation
============

### Python module using pip


If you want to use the python bindings, the easiest way for installation is
usint python-pip:

```
pip install git+https://gitlab.dune-project.org/duneuro/simbiosphere.git
```

### manual installation

Clone the git repository
```bash
git clone --recursive https://gitlab.dune-project.org/duneuro/simbiosphere.git
```
Enter the cloned repository, create a build directory, call cmake and finally
make to compile the library
```bash
cd simbiosphere
mkdir build
cd build
cmake ..
make
```
This will compile the library (and python bindings) which can be found in the
`src` folder of the build directory.

Usage of Python bindings
========================

The module provides a single function through which the analytic solution can
be computed:
```python
import simbiopy

solution = simbiopy.analytic_solution(radii, center, conductivities, electrodes,
                                      dipoleposition, dipolemoment)
```

[1]: https://www.mrt.uni-jena.de/simbio/index.php/Main_Page
[2]: https://sourceforge.net/projects/neurofem-simbio/
[3]: https://github.com/pybind/pybind11
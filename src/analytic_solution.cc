#include <simbio/IP_SIM_SimulatorEEGSpheres_c.h>

#include <simbiosphere/analytic_solution.hh>
#include <simbiosphere/exceptions.hh>

using namespace std;

namespace simbiosphere
{
  namespace detail
  {
    template <class T, size_t N>
    CON_Vector_t<T> makeVector(const array<T, N>& data)
    {
      CON_Vector_t<T> result(N);
      for (unsigned int i = 0; i < N; ++i) {
        result[i] = data[i];
      }
      return result;
    }

    template <class T>
    vector<T> makeVector(const CON_Matrix_t<T>& matrix, size_t column)
    {
      if (column >= matrix.length()) {
        throw IllegalArgumentException("invalid column");
      }
      vector<T> result(matrix.height());
      for (unsigned int i = 0; i < matrix.height(); ++i) {
        result[i] = matrix[i][column];
      }
      return result;
    }

    template <class T, size_t N>
    CON_Matrix_t<T> makeColumnMatrix(const array<T, N>& data)
    {
      CON_Matrix_t<T> result(N, 1);
      for (unsigned int i = 0; i < N; ++i)
        result[i][0] = data[i];
      return result;
    }
  }

  vector<double> analytic_solution(const array<double, 4>& radii, const array<double, 3>& center,
                                   const array<double, 4>& conductivities,
                                   const vector<array<double, 3>>& electrodes,
                                   const array<double, 3>& dipoleposition,
                                   const array<double, 3>& dipolemoment)
  {
    anEEGSensorConfiguration_c configuration;
    configuration.setSensorType(sensortype_EEG);

    for (const auto& electrode : electrodes) {
      configuration.addEEGElectrode("label", detail::makeVector(electrode));
    }

    auto simbioRadii = detail::makeVector(radii);
    auto simbioCenter = detail::makeVector(center);
    auto simbioConducitivities = detail::makeVector(conductivities);
    IP_SIM_SimulatorEEGSpheres_c simulator(configuration, simbioRadii, simbioCenter,
                                           simbioConducitivities);

    CON_Matrix_t<double> simbioResult;
    simulator.computeGainMatrix(1, detail::makeColumnMatrix(dipoleposition),
                                detail::makeColumnMatrix(dipolemoment), simbioResult);
    return detail::makeVector(simbioResult, 0);
  }
}
